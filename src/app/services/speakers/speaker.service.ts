import { Injectable } from '@angular/core';
import { Speaker } from '../../model/speaker';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {Session} from '../../model/session';

@Injectable()
export class SpeakerService {

  private speakersUrl = 'http://localhost:8081/api/v1/speakers';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** Log a SpeakerService message with the MessageService */
  // tslint:disable-next-line:typedef
  private log(message: string) {
    this.messageService.add(`SpeakerService: ${message}`);
  }

  getSpeakers(): Observable<Speaker[]> {
    return this.http.get<Speaker[]>(this.speakersUrl)
      .pipe(
        tap(_ => this.log('fetched speakers')),
        catchError(this.handleError<Speaker[]>('getSpeakers', []))
      );
  }

  getSpeaker(id: number): Observable<Speaker> {
    const url = `${this.speakersUrl}/${id}`;
    return this.http.get<Speaker>(url).pipe(
      tap(_ => this.log(`fetched speaker id=${id}`)),
      catchError(this.handleError<Speaker>(`getSpeaker id=${id}`))
    );
  }

  // PUT request for speaker on the server
  updateSpeaker(speaker: Speaker | undefined): Observable<any> {
    // @ts-ignore
    const id = speaker.id;
    return this.http.put(this.speakersUrl + `/${id}`, speaker, this.httpOptions).pipe(
      // @ts-ignore
      tap(_ => this.log(`updated speaker id=${speaker.id}`)),
      catchError(this.handleError<any>('updateSpeaker'))
    );
  }


  /** POST: add a new speaker to the server */
  createSpeaker(speaker: Speaker): Observable<Speaker> {
    return this.http.post<Speaker>(this.speakersUrl, speaker, this.httpOptions).pipe(
      tap((newSpeaker: Speaker) => this.log(`added hero w/ id=${newSpeaker.id}`)),
      catchError(this.handleError<Speaker>('addSpeaker'))
    );
  }

  public deleteSpeaker(id: number | undefined): Observable<Session> {
    return this.http.delete<Session>(this.speakersUrl + '/' + id, this.httpOptions).pipe(
      tap(_ => this.log(`deleted speaker id=${id}`)),
      catchError(this.handleError<Session>('deleteSpeaker'))
    );
  }



  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  // tslint:disable-next-line:typedef
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
