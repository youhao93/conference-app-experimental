import { Component, OnInit, Inject, Input } from '@angular/core';
import { Session } from '../model/session';
import { SessionService } from '../services/sessions/session.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-session-detail',
  templateUrl: './session-detail.component.html',
  styleUrls: ['./session-detail.component.css']
})

export class SessionDetailComponent implements OnInit {

  @Input() session?: Session;

  public sessionForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private sessionService: SessionService,
    private router: Router
  ) {
    this.sessionForm = new FormGroup({
      session_name: new FormControl('', [Validators.required, Validators.min(0)]),
      session_description: new FormControl('', [Validators.required, Validators.min(0)]),
      session_length: new FormControl('', [Validators.required, Validators.min(0)]),
      session_start: new FormControl('', [Validators.required, Validators.min(0)]),
      session_end: new FormControl('', [Validators.required, Validators.min(0)]),
    });
  }

  ngOnInit(): void {
    this.getSession();
  }

  getSession(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.sessionService.getSession(id)
      .toPromise().then(session =>
    {
      this.session = session;
      console.log(this.session);
    });
  }

  save(): void {
    const id = this.session?.id;
    this.sessionService.updateSession(this.session)
      .toPromise().then(() => this.router.navigate(['/sessions/']));
  }
}


