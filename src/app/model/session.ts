export interface Session {
  id?: number;
  session_name: string;
  session_description?: string;
  session_length: number;
  session_start?: string;
  session_end?: string;
}

