import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ReviewService} from '../services/review/review.service';
import { Review } from '../model/review';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {
  @Output()
  public reviewCreated: EventEmitter<void> | undefined;

  reviews: Review[] = [];

  public reviewForm: FormGroup;

  constructor(private reviewService: ReviewService) {
    this.reviewForm = new FormGroup({
      review_name: new FormControl('', [Validators.required, Validators.min(0)]),
      review_description: new FormControl('', [Validators.required, Validators.min(0)]),
      score: new FormControl('', [Validators.required, Validators.min(0)])
    });
  }

  ngOnInit(): void {
    this.getReviews();

  }

  getReviews(): void {
    this.reviewService.getReviews()
      .toPromise().then(reviews => this.reviews = reviews);
  }

  createReview(): void {
    if (this.reviewForm.valid) {
      const nameField = this.reviewForm.get('review_name');
      const descriptionField = this.reviewForm.get('review_description');
      const scoreField = this.reviewForm.get('score');
      if (nameField && descriptionField && scoreField) {
        this.reviewService.createReview({
          review_name: nameField.value,
          review_description: descriptionField.value,
          score: scoreField.value
        }).toPromise().then(() => {
          this.reviewForm.reset();
          this.getReviews();
        });
      }
    }
  }

  delete(review: Review): void {
    this.reviews = this.reviews.filter(r => r !== review);
    this.reviewService.deleteReview(review.id).subscribe();
  }

}
