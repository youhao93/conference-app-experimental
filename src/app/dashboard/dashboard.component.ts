import { Component, OnInit } from '@angular/core';
import { Session } from '../model/session';
import { SessionService } from '../services/sessions/session.service';
import { SpeakerService } from '../services/speakers/speaker.service';

import {Speaker} from '../model/speaker';
import { Review } from '../model/review';
import {ReviewService} from '../services/review/review.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  sessions: Session[] = [];

  speakers: Speaker[] = [];

  reviews: Review[] = [];


  constructor(
    private sessionService: SessionService,
    private speakerService: SpeakerService,
    private reviewService: ReviewService
  ) { }

  ngOnInit(): void {
    this.getSessions();
    this.getSpeakers();
    this.getReviews();
  }

  getSessions(): void {
    this.sessionService.getSessions()
      .toPromise().then(sessions => this.sessions = sessions);
  }

  getSpeakers(): void {
    this.speakerService.getSpeakers()
      .toPromise().then(sessions => this.speakers = sessions);
  }

  getReviews(): void {
    this.reviewService.getReviews()
      .toPromise().then(reviews => this.reviews = reviews);
  }

}
