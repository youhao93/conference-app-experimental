import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SessionsComponent } from './sessions/sessions.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SessionDetailComponent } from './session-detail/session-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import {SessionService} from './services/sessions/session.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SpeakersComponent} from './speakers/speakers.component';
import {SpeakerService} from './services/speakers/speaker.service';
import { SpeakerDetailComponent } from './speaker-detail/speaker-detail.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { ReviewDetailComponent } from './review-detail/review-detail.component';
import {ReviewService} from './services/review/review.service';
import { ClarityModule } from '@clr/angular';


@NgModule({
  declarations: [
    AppComponent,
    SessionsComponent,
    SessionDetailComponent,
    SpeakersComponent,
    MessagesComponent,
    DashboardComponent,
    SpeakerDetailComponent,
    ReviewsComponent,
    ReviewDetailComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ClarityModule
  ],
  providers: [SessionService, SpeakerService, ReviewService],
  bootstrap: [AppComponent]
})
export class AppModule { }
