import { Injectable } from '@angular/core';
import { Session } from '../../model/session';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';

@Injectable()
export class SessionService {

  private sessionsUrl = 'http://localhost:8080/api/v1/sessions';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** Log a SessionService message with the MessageService */
  // tslint:disable-next-line:typedef
  private log(message: string) {
    this.messageService.add(`SessionService: ${message}`);
  }

  getSessions(): Observable<Session[]> {
    return this.http.get<Session[]>(this.sessionsUrl)
      .pipe(
        tap(_ => this.log('fetched sessions')),
        catchError(this.handleError<Session[]>('getSessions', []))
      );
  }

  getSession(id: number): Observable<Session> {
    const url = `${this.sessionsUrl}/${id}`;
    return this.http.get<Session>(url).pipe(
      tap(_ => this.log(`fetched session id=${id}`)),
      catchError(this.handleError<Session>(`getSession id=${id}`))
    );
  }

  // PUT request for session on the server
  updateSession(session: Session | undefined): Observable<any> {
    // @ts-ignore
    const id = session.id;
    return this.http.put(this.sessionsUrl + `/${id}`, session, this.httpOptions).pipe(
      // @ts-ignore
      tap(_ => this.log(`updated session id=${session.id}`)),
      catchError(this.handleError<any>('updateSession'))
    );
  }


  /** POST: add a new session to the server */
  createSession(session: Session): Observable<Session> {
    return this.http.post<Session>(this.sessionsUrl, session, this.httpOptions).pipe(
      tap((newSession: Session) => this.log(`added session w/ id=${newSession.id}`)),
      catchError(this.handleError<Session>('addSession'))
    );
  }


  public deleteSession(id: number | undefined): Observable<Session> {
    return this.http.delete<Session>(this.sessionsUrl + '/' + id, this.httpOptions).pipe(
      tap(_ => this.log(`deleted session id=${id}`)),
      catchError(this.handleError<Session>('deleteSession'))
    );
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  // tslint:disable-next-line:typedef
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
